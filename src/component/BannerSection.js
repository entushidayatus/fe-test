import React, { Component } from 'react'
import PropTypes from 'prop-types'

let propTypes = {
    title: PropTypes.string
  }
  
  export default class BannerSection extends Component {
    render() { 
      return (
        <div>
          <section className="banner_area">
            <div className="banner_inner d-flex align-items-center">
              <div className="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
              <div className="container">
                <div className="banner_content text-center">
                  <h2>{this.props.title}</h2>
                </div>
              </div>
            </div>
          </section>
        </div>
      );
    }
  }
   
  BannerSection.propTypes = propTypes;