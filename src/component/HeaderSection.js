import React, { Component } from 'react'
import history from '../history'
import {Accordion,Button} from 'react-bootstrap'
import $ from 'jquery'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded: false
        }
    }
    
    componentDidMount() {
        $('html, body').animate({scrollTop: '0px'}, 100)
        let nav_offset_top = $('header').height() + 50; 
        if ( $('.header_area').length ){ 
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();   
                if (scroll >= nav_offset_top ) {
                    $(".header_area").addClass("navbar_fixed");
                } else {
                    $(".header_area").removeClass("navbar_fixed");
                }
            });
        };
    }
    
    clickBurgerMenu() {
        this.setState({
            isExpanded: !this.state.isExpanded
        })
    }

    render() {
        return (
            <div>
                <header className="header_area">
                    <div className="main_menu" id="mainNav">
                        <nav className="navbar navbar-expand-lg navbar-light">
                            <Accordion className="container">
                                <a className="navbar-brand logo_h" href="/#"><img src={require("../assets/img/logo.png")} alt=""/><img src="img/logo-2.png" alt=""/></a>
                                <Accordion.Toggle as={Button.Link} 
                                    className="navbar-toggler" 
                                    aria-expanded={this.state.isExpanded}
                                    onClick={() => this.clickBurgerMenu()}
                                    eventKey="navbarSupportedContent">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </Accordion.Toggle>
                                <Accordion.Collapse className="navbar-collapse offset" eventKey="navbarSupportedContent">
                                    <ul className="nav navbar-nav menu_nav ml-auto">
                                        <li className="nav-item active" style={{cursor: 'pointer'}}>
                                            <p className="nav-link" onClick={() => history.push('/')}>Home</p>
                                        </li>  
                                        <li className="nav-item" style={{cursor: 'pointer'}}>
                                            <p className="nav-link" onClick={() => history.push('/film')}>Film</p>
                                        </li> 
                                        <li className="nav-item" style={{cursor: 'pointer'}}>
                                            <p className="nav-link" onClick={() => history.push('/contact')}>Contact</p>
                                        </li> 
                                    </ul>
                                </Accordion.Collapse> 
                            </Accordion>
                        </nav>
                    </div>
                </header>
            </div>
        );
    }
}

export default Header;