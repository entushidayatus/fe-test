import React, { Component } from 'react';

class FooterSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {}
        }
    }

    handleValidation =()=> {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if(!fields["email"]){
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }
    
        if(typeof fields["email"] !== "undefined"){
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');
    
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
            formIsValid = false;
            errors["email"] = "Email is not valid";
            }
        }
    
        this.setState({errors: errors});
        return formIsValid;
    }

    handleChange =(field, e)=> {
        let fields = this.state.fields
        fields[field] = e.target.value
        this.setState({ fields })
        console.log(this.state.fields);
    }

    handleSubmit =(e)=> {
        e.preventDefault()
        if(this.handleValidation()){
            alert("Thank you for subscribe")
        }else{
            alert("Opps.. Sorry, please check your email.")
        }
    }

    render() {
        return (
            <div>
                 <footer className="footer_area p_120">
                    <div className="container">
                        <div className="row footer_inner">
                            <div className="col-lg-5 col-sm-6">
                                <aside className="f_widget ab_widget">
                                    <div className="f_title">
                                        <h3>About Me</h3>
                                    </div>
                                    <p>Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills,</p>
                                </aside>
                            </div>
                            <div className="col-lg-5 col-sm-6">
                                <aside className="f_widget news_widget">
                                    <div className="f_title">
                                        <h3>Newsletter</h3>
                                    </div>
                                    <p>Stay updated with our latest trends</p>
                                    <div id="mc_embed_signup">
                                        <form onSubmit={this.handleSubmit} method="get" className="subscribe_form relative">
                                            <div className="input-group d-flex flex-row">
                                                <input type="text" ref="email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} className="form-control" id="email" placeholder="Enter email address" />
                                                
                                                <button className="btn sub-btn"><span className="lnr lnr-arrow-right"></span></button>		
                                            </div>				
                                            <div className="mt-10 info"></div>
                                        </form>
                                    </div>
                                </aside>
                            </div>
                            <div className="col-lg-2">
                                <aside className="f_widget social_widget">
                                    <div className="f_title">
                                        <h3>Follow Me</h3>
                                    </div>
                                    <p>Let us be social</p>
                                    <ul className="list">
                                        <li><a href="/#"><i className="fa fa-facebook"></i></a></li>
                                        <li><a href="/#"><i className="fa fa-twitter"></i></a></li>
                                        <li><a href="/#"><i className="fa fa-dribbble"></i></a></li>
                                        <li><a href="/#"><i className="fa fa-behance"></i></a></li>
                                    </ul>
                                </aside>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default FooterSection;