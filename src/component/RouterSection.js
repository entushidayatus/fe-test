import React, { Component } from 'react'
import {Router, Switch, Route} from 'react-router-dom'
import history from '../history'

// @Load Page
import Home from '../page/Home'
import Film from '../page/Film'
import FilmDetail from '../page/FilmDetail'
import NotFound from '../page/Page404'
import Contact from '../page/Contact'

class RouterSection extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path={"/"} exact render={(props)=> {return <Home {...props} />}}/>
                    <Route path={"/film/:filmId"} render={(props)=> {return <FilmDetail {...props} />}}/>
                    <Route path={"/film"} render={(props)=> {return <Film {...props} />}}/>
                    <Route path={"/contact"} render={(props)=> {return <Contact {...props} />}}/>
                    <Route path={"/*"} render={(props) => {return <NotFound {...props} />}} />
                </Switch>
            </Router>
        );
    }
}

export default RouterSection;