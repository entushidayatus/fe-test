import React, { Component } from 'react'
import BannerSection from '../component/BannerSection'
import Axios from 'axios'
import ContactSection from './ContactSection';

class FilmDetailSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            film: {
                id: '',
                title: '',
                description: '',
                director: '',
                producer: '',
                release_date: '',
                rt_score: '',
                url: ''
            }
        }
    }

    componentDidMount =()=> {
        this.getDetailFilm()
    }

    getDetailFilm =()=> {
        let id = this.props.filmId
        Axios.get('https://ghibliapi.herokuapp.com/films/'+id)
        .then((res) => {
            let film = res.data
            this.setState({
                film: {
                    id: film.id,
                    title: film.title,
                    description: film.description,
                    director: film.director,
                    producer: film.producer,
                    release_date: film.release_date,
                    rt_score: film.rt_score,
                    url: film.url
                }
            })
        })
    }

    render() {
        if(this.state.film.id !== ""){
        return (
            <div>
            <BannerSection title={this.state.film.title}/>
                <section className="blog_area single-post-area p_120">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 posts-list">
                                <div className="single-post row">
                                    <div className="col-lg-12">
                                        <div className="feature-img">
                                            <img className="img-fluid" src="img/blog/feature-img1.jpg" alt="" />
                                        </div>									
                                    </div>
                                    <div className="col-lg-3  col-md-3">
                                        <div className="blog_info text-right">
                                            <div className="post_tag">
                                            </div>
                                            <ul className="blog_meta list">
                                                <li>
                                                    <a href="/#">{this.state.film.director}<i className="lnr lnr-user"></i></a>
                                                </li>
                                                <li>
                                                    <a href="/#">{this.state.film.release_date}<i className="lnr lnr-calendar-full"></i></a>
                                                </li>
                                                <li>
                                                    <a href="/#">{this.state.film.rt_score}<i className="lnr lnr-star"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-md-9 blog_details">
                                        <h2>{this.state.film.title}</h2>
                                        <p className="excert">
                                            {this.state.film.description}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="blog_right_sidebar">
                                    <aside className="single_sidebar_widget author_widget">
                                        <img className="author_img rounded-circle" src="img/blog/author.png" alt="" />
                                        <h4>{this.state.film.director}</h4>
                                        <div className="social_icon">
                                            <a href="/#"><i className="fa fa-facebook"></i></a>
                                            <a href="/#"><i className="fa fa-twitter"></i></a>
                                            <a href="/#"><i className="fa fa-github"></i></a>
                                            <a href="/#"><i className="fa fa-behance"></i></a>
                                        </div>
                                        <div className="br"></div>
                                    </aside>
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <ContactSection/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
        } else {
            return (
                <div>
                    <section className="blog_area single-post-area p_120">
                        <div className="container">
                            <div className="impress_inner text-center">
                                <img style={{objectFit: 'contain'}} src={require('../assets/img/loader.gif')} alt="" />
                            </div>
                        </div>
                    </section>
                </div>
            )
        }
    }
}

export default FilmDetailSection;