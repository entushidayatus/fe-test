import React, { Component } from 'react'
import Axios from 'axios'
import lodash from 'lodash'
import history from '../history'

class FilmSection extends Component {
    constructor() {
        super();
        this.state = {
            dataFilm: [],
            offset: 0,
            perSection: 3,
            pageCount: null
        }
    }

    componentDidMount =()=> {
        this.getDataFilm()
    }

    getDataFilm =()=> {
        Axios.get('https://ghibliapi.herokuapp.com/films')
        .then((res) => {
            let data = res.data
            let slices = data.slice(this.state.offset, this.state.offset + this.state.perSection)
            let listPerSection = slices.map((film, i) => 
                <div className="col-lg-4" key={i}>
                    <div className="furniture_item">

                        <h4 filmId={film.id} onClick={() => history.push('/film/'+film.id)} style={{cursor: 'pointer'}}>{film.title}</h4>
                        <p>{lodash.truncate(film.description, {'length': 60, 'separator': ' '})}</p>   
                    </div>
                </div>
            )
            this.setState({
                pageCount: Math.ceil(data.length / this.state.perSection),
                listPerSection
            })
        })
    }

    render() {
        if(this.props.filmId !== ""){
            let list = this.state.listPerSection
            return (
                <div>  
                    <section className="furniture_area p_120">
                        <div className="container">
                            <div className="main_title">
                                <h2>Section Film</h2>
                            </div>
                            <div className="furniture_inner row" >
                                {list}
                                <div className="container">
                                    <div className="impress_inner text-center">
                                        <p onClick={() => history.push('/film')} style={{cursor: 'pointer'}} className="main_btn">View More</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            );
        } else {
            return (
                <div>
                    <section className="blog_area single-post-area p_120">
                        <div className="container">
                            <div className="impress_inner text-center">
                                <img style={{objectFit: 'contain'}} src={require('../assets/img/loader.gif')} alt="" />
                            </div>
                        </div>
                    </section>
                </div>
            )
        }
    }
}

export default FilmSection;