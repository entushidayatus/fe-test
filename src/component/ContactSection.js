import React, { Component } from 'react';

class ContactSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            dataName: ''
        }
    }
    
    handleValidation =()=> {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if(!fields["name"]){
            formIsValid = false;
            errors["name"] = "Cannot be empty";
        }
    
        if(typeof fields["name"] !== "undefined"){
            if(!fields["name"].match(/^[a-zA-Z]+$/)){
            formIsValid = false;
            errors["name"] = "Only letters";
            }      	
        }
    
        if(!fields["email"]){
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        }
    
        if(typeof fields["email"] !== "undefined"){
            let lastAtPos = fields["email"].lastIndexOf('@');
            let lastDotPos = fields["email"].lastIndexOf('.');
    
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
            formIsValid = false;
            errors["email"] = "Email is not valid";
            }
        }
    
        this.setState({errors: errors});
        return formIsValid;
    }

    handleSubmit =(e)=> {
        e.preventDefault()
        if(this.handleValidation()){
            this.setState({
                dataName: this.state.fields
            })
            alert("Thank you for submitted your data")
        }else{
            alert("Opps.. Sorry, please check your data.")
        }
    }

    handleChange =(field, e)=> {
        let fields = this.state.fields
        fields[field] = e.target.value
        this.setState({ fields })
    }
    
    render() {
        let nameInput = this.state.dataName.name
        console.log("Nama anda: ", nameInput);
        return (
            <div>
                <section className="blog_area single-post-area p_120">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 posts-list">
                                <div className="comment-form">
                                    <h4>Contact Us</h4>
                                    <form onSubmit={this.handleSubmit.bind(this)}>
                                        <div className="form-group form-inline">
                                        <div className="form-group col-lg-6 col-md-6 name">
                                            <input type="text" ref="name" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]} className="form-control danger" id="name" placeholder="Enter Name" />
                                            <label for="name" className="error">{this.state.errors["name"]}</label>
                                        </div>
                                        <div className="form-group col-lg-6 col-md-6 email">
                                            <input type="text" ref="email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} className="form-control" id="email" placeholder="Enter email address" />
                                            <label for="email" className="error">{this.state.errors["email"]}</label>
                                        </div>										
                                        </div>
                                        <div className="form-group">
                                            <textarea onChange={this.handleChange.bind(this, "message")} value={this.state.fields["message"]}  className="form-control mb-10" rows="5" name="message" placeholder="Messege" required=""></textarea>
                                        </div>
                                        <input type="submit" className="primary-btn submit_btn" value="Submit"/>	
                                    </form>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default ContactSection;