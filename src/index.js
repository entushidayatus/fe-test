import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

// load style
import './assets/css/animate.css'
import './assets/css/404.css'
import './assets/css/style.css'
import './assets/css/responsive.css'
import './assets/css/font-awesome.min.css'
import './assets/css/bootstrap.css'
import './assets/vendors/flaticon/flaticon.css'
import './assets/vendors/linericon/style.css'
import './assets/css/magnific-popup.css'

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
