import React, { Component } from 'react';
import HeaderSection from '../component/HeaderSection'
import BannerSection from '../component/BannerSection';
import FooterSection from '../component/FooterSection';
import ContactSection from '../component/ContactSection';

class Contact extends Component {
    render() {
        return (
            <div>
                <HeaderSection/>
                <BannerSection title="Contact"/>
                <ContactSection/>
                <FooterSection/>
            </div>
        );
    }
}

export default Contact;