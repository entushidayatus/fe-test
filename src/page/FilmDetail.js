import React, { Component } from 'react'
import HeaderSection from '../component/HeaderSection'
import FooterSection from '../component/FooterSection'
import FilmDetailSection from '../component/FilmDetailSection'

class FilmDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div>
                <HeaderSection/>
                <FilmDetailSection filmId={this.props.match.params.filmId} />
                <FooterSection/>
            </div>
        );
    }
}

export default FilmDetail;