import React, { Component } from 'react'
import Axios from 'axios'
import lodash from 'lodash'
import history from '../history'
import ReactPaginate from 'react-paginate'
import HeaderSection from '../component/HeaderSection'
import BannerSection from '../component/BannerSection'
import FooterSection from '../component/FooterSection'

class Film extends Component {
    constructor() {
        super();
        this.state = {
            dataFilm: [],
            offset: 0,
            perPage: 5,
            currentPage: 0,
            pageCount: null
        }
    }

    getDataFilm =()=> {
        Axios.get('https://ghibliapi.herokuapp.com/films')
        .then((res) => {
            let data = res.data
            let slices = data.slice(this.state.offset, this.state.offset + this.state.perPage)
            let listFilm = slices.map((film, i) => 
            <div className="blog_left_sidebar" key={i}>
                <article className="row blog_item">
                    <div className="col-md-12">
                        <div className="blog_post">
                            <img src="assets/img/blog/main-blog/m-blog-1.jpg" alt=""/>
                            <div className="blog_details">
                                <h2 onClick={() => history.push('/film/'+film.id)} style={{cursor: 'pointer'}}>{film.title}</h2>
                                <p>{lodash.truncate(film.description, {'length': 100, 'separator': ' '})}</p>
                                <p onClick={() => history.push('/film/'+film.id)} style={{cursor: 'pointer'}} className="blog_btn">View More</p>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            )
            this.setState({
                pageCount: Math.ceil(data.length / this.state.perPage),
                listFilm
            })
        })
    }

    handlePageClick =(e)=> {
        let selectedPage = e.selected
        let offset = selectedPage * this.state.perPage
        this.setState({
            currentPage: selectedPage,
            offset: offset
        }, () => {
            this.getDataFilm()
        });
    }
    
    componentDidMount =()=> {
        this.getDataFilm()
    }

    render() {
        if(this.state.dataFilm !== 0){
            let film = this.state.listFilm
            return (
                <div>  
                    <HeaderSection/>
                    <BannerSection title="Section Film"/>
                    <section className="blog_area">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    {film}

                                    <nav className="blog-pagination justify-content-center d-flex">
                                        <ReactPaginate
                                            previousLabel={"Prev"}
                                            nextLabel={"Next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={this.state.pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"}/>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </section>
                    <FooterSection/>
                </div>
            );
        } else {
            return (
              <div>
                <img style={{objectFit: 'contain'}} src={require('../assets/img/loader.gif')} alt="" />
              </div>
            )
        }
    }
}

export default Film;