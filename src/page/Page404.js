import React, { Component } from 'react';
import history from '../history';
import '../assets/css/404.css';

class Page404 extends Component {
    clickBack = () => {
        history.push('/')
    }

    render() {
        return (
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        <h1>4<span></span>4</h1>
                    </div>
                    <h2>Oops! Page Not Be Found</h2>
                    <p>Sorry but the page you are looking for does not exist, have been removed.
                        name changed or is temporarily unavailable</p>
                    <span onClick={this.clickBack}>Back to homepage</span>
                </div>
            </div>
        );
    }
}

export default Page404;