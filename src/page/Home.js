import React, { Component } from 'react'
import HeaderSection from '../component/HeaderSection'
import FilmSection from '../component/FilmSection'
import BannerSection from '../component/BannerSection'
import FooterSection from '../component/FooterSection'

class Home extends Component {
    render() {
        return (
            <div>
                <HeaderSection/>
                <BannerSection/>
                <FilmSection/>
                <FooterSection/>
            </div>
        );
    }
}

export default Home;