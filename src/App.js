import React from 'react'
import history from './history'

// @load components
import RouterSection from './component/RouterSection';

function App() {
  return (
    <RouterSection history={history} />
  );
}

export default App;
